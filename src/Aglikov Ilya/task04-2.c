#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define N 5
#define M 256

char str[N][M] = {'\0'};
int i = 0;

int main()
{
    for (i = 0; i<N; i++)
    {
        fgets(str[i], M, stdin);
        if (str[i][0] == '\0')
            break;
    }
    i--;
    srand(time(NULL));
    printf("\n");
    fputs(str[rand()%i], stdout);
    return 0;
}
